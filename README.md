Teste Via Varejo
===============
Este é o teste executado por Daniel Pintscher Baptista, atual frontend da empresa Linx em magento 2.1.

Para iniciar a aplicação:

```
npm install
```

Após fazer as instalações:

```
npm start
```

Para gerar os estilos a partir do less:

```
gulp less
```
