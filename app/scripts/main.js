$(document).ready(function(){

	var categoriesContent = "";

	$.get('./api/side_bar.json', function(data){
		console.log(data);
		$.each(data, function(i, e){
			categoriesContent += '<div class="side-categories_title"><a href="' + e.action + '" target="_self">' + e.category + "</a></div>";
			if(e.menus.length>0){
				categoriesContent += '<ul class="side-categories_content">';
				$.each(e.menus, function(mi, me){
					categoriesContent += '<li class="side-category_title">' + me.name + '</li>';
					if(me.itens.length>0){
						$.each(me.itens, function(mii, mie){
							categoriesContent += '<li class="side-category_name"><a href="' + mie.action + '" target="_self">' + mie.name + '(' + mie.count + ')' + '</a></li>';
						});
					}
				});
				categoriesContent += '</ul>';
			}
		});
		$('.side-categories').html(categoriesContent);
	});

	$('.products-list').slick({
		infinite: false,
		slidesToShow: 3,
		slidesToScroll: 3
	});

});